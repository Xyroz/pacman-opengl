#version 430 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;
layout (location = 2) in vec2 texCoord;

out vec2 TexCoord;
out vec3 vertexColor;

uniform mat4 view;
uniform mat4 projection;
uniform mat4 model;

void main() {
   gl_Position = projection * view * model * vec4(position, 1.0);
   vertexColor = color;
   TexCoord = vec2(texCoord.x, texCoord.y);
}