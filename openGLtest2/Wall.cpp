#include "Wall.h"
#include "logger.h"
#include "Shaders.h"
#include "Dependencies\glm\glm\gtc\matrix_transform.hpp"
#include "Dependencies\glm\glm\gtc\type_ptr.hpp"

extern GLFWwindow* window;
extern int vertexPerTriangle;
extern Shader triangleShader;

Wall::Wall(const glm::vec3 pos, const GLfloat size, const GLuint textureID)
{   
    position = pos;
    texture = textureID;
    gridPos = glm::vec2(pos.x, pos.y);

    GLfloat vertices[] = {
         1.0f,  1.0f,   0.f, // Top right
         1.0f,   0.f,   0.f, // Bottom Right
          0.f,   0.f,   0.f, // Bottom Left
          0.f,  1.0f,   0.f  // Top Left
    };

    GLfloat color[] = {
        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 0.0f
    };

    GLfloat texCoord[] = {
        1.0f, 0.0f,
        1.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 1.0f
    };

    // HUSK GLint OG IKKE GLfloat!!!!!!
    GLint indices[] = {
        0, 1, 3,
        1, 2, 3
    };

    verticesNo = sizeof(vertices);
    indicesNo = sizeof(indices);

    glGenVertexArrays(1, &VAO);                                                     // Generates a new vertex array object
    glBindVertexArray(VAO);                                                         // Binds it for use

    glGenBuffers(1, &VBO);                                                          // Generates a new buffer object
    glBindBuffer(GL_ARRAY_BUFFER, VBO);                                             // Binds it for use to GL_ARRAY_BUFFER
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices) + sizeof(color) + sizeof(texCoord), NULL, GL_STATIC_DRAW);// Puts data in GL_ARRAY_BUFFER

    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);                // Sets vertices data
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices), sizeof(color), color);       // Sets color data
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices) + sizeof(color), sizeof(texCoord), texCoord);
    // Sets texture coordinates

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);                   // Points to the vertices to use 
    glEnableVertexAttribArray(0);                                                   // Enables the pointer

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(vertices))); // Points to the color to use
    glEnableVertexAttribArray(1);                                                   // Enables

    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(vertices) + sizeof(color)));// Points to the texture coordinates
    glEnableVertexAttribArray(2);                                                   // Anables

    glGenBuffers(1, &EBO);                                                          // Generate buffer for EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);                                     // Binds EBO for use
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);// Inserts indices data into EBO buffer
}

void Wall::display()
{
    model = glm::mat4(1.0f);
    model = glm::translate(model, position);

    triangleShader.setMatrix("model", model);

    glBindTexture(GL_TEXTURE_2D, texture);                                          // Binds texture for use
    glBindVertexArray(VAO);                                                         // Binds vertex array object for use
    glDrawElements(GL_TRIANGLES, indicesNo, GL_UNSIGNED_INT, 0);                    // Draws triangles based on indices and coordinates;
}

glm::vec2 Wall::getGridPosition()
{
    return gridPos;
}