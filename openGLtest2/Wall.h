#pragma once
#include <iostream>
#include <vector>

#include "Dependencies\glew-2.1.0\include\GL\glew.h"
#include "Dependencies\glfw-3.2.1.bin.WIN32\include\GLFW\glfw3.h"
#include "Dependencies\STB_image\stb_image.h"
#include "Dependencies\glm\glm\glm.hpp"

class Wall
{
private:
    GLuint VAO;
    GLuint VBO;
    GLuint texture;
    GLuint EBO;
    int verticesNo;
    int indicesNo;
    glm::vec3 position;
    glm::mat4 model;
    glm::vec2 gridPos;
public:
    Wall(const glm::vec3 pos, const GLfloat size, const GLuint textureID);
    glm::vec2 getGridPosition();
    void display();
};
