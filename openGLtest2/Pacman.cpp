#include "Pacman.h"
#include "Dependencies\glm\glm\gtc\matrix_transform.hpp"
#include "Shaders.h"
#include "logger.h"
#include <cmath>

extern Shader triangleShader;
extern int speed;

Pacman::Pacman(const glm::vec3 pos, const GLuint textureID, const std::vector<std::vector<int>> walls)
{
    crashing = false;

    position = pos;
    texture = textureID;
    tiles = walls;
    direction = glm::vec3(0, 0, 0);
    newDirection = glm::vec3(0, 0, 0);
    /*
    for (int i = 0; i < tiles.size(); i++)
    {
        for (int j = 0; j < tiles[i].size(); j++)
        {
            std::cout << tiles[i][j] << " ";
        }
        std::cout << "\n";
    }*/

    GLfloat vertices[] = {
        1.0f,  1.0f,   0.f, // Top right
        1.0f,   0.f,   0.f, // Bottom Right
        0.f,   0.f,   0.f, // Bottom Left
        0.f,  1.0f,   0.f  // Top Left
    };

    GLfloat texCoord[] = {
        1.0f, 0.0f,
        1.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 1.0f
    };

    GLint indices[] = {
        0, 1, 3,
        1, 2, 3
    };

    verticesNo = sizeof(vertices);
    indicesNo = sizeof(indices);

    glGenVertexArrays(1, &VAO);                                                     // Generates a new vertex array object
    glBindVertexArray(VAO);                                                         // Binds it for use

    glGenBuffers(1, &VBO);                                                          // Generates a new buffer object
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices) + sizeof(texCoord), NULL, GL_STATIC_DRAW);
    
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices), sizeof(texCoord), texCoord);
    
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(2);

    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

}

void Pacman::display()
{
    model = glm::mat4(1.0f);
    model = glm::translate(model, position);

    triangleShader.setMatrix("model", model);

    glBindTexture(GL_TEXTURE_2D, texture);
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, indicesNo, GL_UNSIGNED_INT, 0);
}

void Pacman::changeDirection(const char dir, double deltaTime)
{

    //DEBUGPOSITION();
    switch (dir)
    {
    case 'U':
        if (tiles[std::round(position.y + 1)][std::round(position.x)] != 1)
            LOG_DEBUG("Up case, tile info: %d", tiles[position.y + 1][position.x]);
            DEBUGPOSITION();
            newDirection = glm::vec3(0.0f, 1.0f * deltaTime * speed, 0.0f);
            crashing = false;
        break;
    case 'D':
        if (tiles[std::round(position.y - 1)][std::round(position.x)] != 1)
            LOG_DEBUG("Down case, tile info: %d", tiles[position.y - 1][position.x]);
            DEBUGPOSITION();
            newDirection = glm::vec3(0.0f, -1.0f * deltaTime * speed, 0.0f);
            crashing = false;
        break;
    case 'L':
        if (tiles[std::round(position.y)][std::round(position.x - 1)] != 1)
            LOG_DEBUG("Left case, tile info: %d", tiles[position.y][position.x - 1]);
            DEBUGPOSITION();
            newDirection = glm::vec3(-1.0f * deltaTime *  speed, 0.0f, 0.0f);
            crashing = false;
        break;
    case 'R':
        if (tiles[std::round(position.y)][std::round(position.x + 1)] != 1)
            LOG_DEBUG("Right case, tile info: %d", tiles[position.y][position.x + 1]);
            DEBUGPOSITION();
            newDirection = glm::vec3(1.0f * deltaTime * speed, 0.0f, 0.0f);
            crashing = false;
        break;
    }
}

void Pacman::move(double deltaTime)
{
    direction = newDirection;

    if (position.x < 0.0f)
        position = glm::vec3(27.0f, position.y, 0.0f);
    else if (position.x > 27.0f)
        position = glm::vec3(0.0f, position.y, 0.0f);
    else if (tiles[std::ceil(position.y + direction.y)][std::ceil(position.x + direction.x)] == 1)
        direction = glm::vec3(0.0f, 0.0f, 0.0f);

    position += direction;
}

void Pacman::DEBUGPOSITION()
{
    LOG_DEBUG("%f", position.x);
    LOG_DEBUG("%f", position.y);
}