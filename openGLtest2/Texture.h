#pragma once

#include "Dependencies\glew-2.1.0\include\GL\glew.h"

class Texture
{
private:
    GLuint textureID;
public:
    Texture(const char* path);
    GLuint getID();
};