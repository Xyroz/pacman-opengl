#pragma once
#include <iostream>
#include <vector>
#include "Shaders.h"
#include "mad.h"
#include "Wall.h"

#include "Dependencies\glew-2.1.0\include\GL\glew.h"
#include "Dependencies\glfw-3.2.1.bin.WIN32\include\GLFW\glfw3.h"

bool initWindow();
bool initShader(Shader& shader, const char* vertexPath, const char* fragmentPath);
bool initMap(const char* mapPath, std::vector<Wall>& walls, GLuint texture);
void windowsize_callback(GLFWwindow* window, int width, int height);