#pragma once
#include <string>
#include <vector>
#include "Wall.h"

#include "Dependencies\glew-2.1.0\include\GL\glew.h"
#include "Dependencies\glm\glm\glm.hpp"

class Pacman
{
private:
    GLuint VAO;
    GLuint VBO;
    GLuint EBO;
    GLuint texture;
    int verticesNo;
    int indicesNo;
    glm::vec3 position;
    glm::vec3 direction;
    glm::vec3 newDirection;
    glm::mat4 model;
    std::vector<std::vector<int>> tiles;
    bool crashing;
public:
    Pacman(const glm::vec3 pos, const GLuint textureID, const std::vector<std::vector<int>> walls);
    void display();
    void changeDirection(const char dir, double deltaTime);
    void move(double deltaTime);
    void DEBUGPOSITION();
};