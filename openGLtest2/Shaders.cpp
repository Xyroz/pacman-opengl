#include "Shaders.h"
#include "logger.h"

bool Shader::createProgram(const std::string& vertexString, const std::string& fragmentString)
{
    const char* vShaderCode = vertexString.c_str();
    const char* fShaderCode = fragmentString.c_str();

    // Create and compile vertex shader
    GLuint vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, NULL);
    glCompileShader(vertex);

    int success;
    char infoLog[512];

    // Catch error
    glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertex, 512, NULL, infoLog);
        LOG_ERROR("ERROR::SHADER::VERTEX::COMPILATION_FAILED...\n%s", infoLog);
        return false;
    }

    LOG_DEBUG("Compiled vertex shader");

    // Create and compile fragment shader
    GLuint fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderCode, NULL);
    glCompileShader(fragment);

    // Catch error
    glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragment, 512, NULL, infoLog);
        LOG_ERROR("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED...\n%s", infoLog);
        return false;
    }

    LOG_DEBUG("Compiled fragment shader");

    // Creates shader program, and attaches and links it
    programNo = glCreateProgram();
    glAttachShader(programNo, vertex);
    glAttachShader(programNo, fragment);
    glLinkProgram(programNo);

    // Catch error
    glGetProgramiv(programNo, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(programNo, 512, NULL, infoLog);
        LOG_ERROR("ERROR::SHADER::PROGRAM::LINK_FAILED...\n%s", infoLog);
        return false;
    }

    LOG_DEBUG("Attached shaders to shaderprogram");

    // Deletes vertex and fragment since they are now linked to shader program
    glDeleteShader(vertex);
    glDeleteShader(fragment);
    return true;
}

void Shader::use()
{
    glUseProgram(programNo);
}

void Shader::setInt(const std::string& name, GLint value) 
{
    use();
    glUniform1i(glGetUniformLocation(programNo, name.c_str()), value);
}

void Shader::setInt(const std::string& name, GLint value, GLint value2)
{
    use();
    glUniform2i(glGetUniformLocation(programNo, name.c_str()), value, value2);
}

void Shader::setInt(const std::string& name, GLint value, GLint value2, GLint value3)
{
    use();
    glUniform3i(glGetUniformLocation(programNo, name.c_str()), value, value2, value3);
}

void Shader::setInt(const std::string& name, GLint value, GLint value2, GLint value3, GLint value4)
{
    use();
    glUniform4i(glGetUniformLocation(programNo, name.c_str()), value, value2, value3, value4);
}

void Shader::setFloat(const std::string& name, GLfloat value)
{
    use();
    glUniform1f(glGetUniformLocation(programNo, name.c_str()), value);
}

void Shader::setFloat(const std::string& name, GLfloat value, GLfloat value2)
{
    use();
    glUniform2f(glGetUniformLocation(programNo, name.c_str()), value, value2);
}
void Shader::setFloat(const std::string& name, GLfloat value, GLfloat value2, GLfloat value3)
{
    use();
    glUniform3f(glGetUniformLocation(programNo, name.c_str()), value, value2, value3);
}

void Shader::setFloat(const std::string& name, GLfloat value, GLfloat value2, GLfloat value3, GLfloat value4)
{
    use();
    glUniform4f(glGetUniformLocation(programNo, name.c_str()), value, value2, value3, value4);
}

void Shader::setMatrix(const std::string& name, glm::mat4 trans)
{
    use();
    glUniformMatrix4fv(glGetUniformLocation(programNo, name.c_str()), 1, GL_FALSE, glm::value_ptr(trans));
}
