#include "Init.h"
#include "logger.h"
#include <vector> 

extern GLFWwindow* window;
extern glm::mat4 view;
extern glm::mat4 projection;
extern Shader triangleShader;
extern std::vector<std::vector<int>> tileMap;

bool initWindow()
{
    glfwInit();                                                             // Inits glfw
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);                          // Sets major openGL version
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);                          // Sets minor openGL version
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);          // Sets that we want to use core functionality

    window = glfwCreateWindow(600, 800, "Learning 2", NULL, NULL);          // Creates a window, size 800x600, Learning 2 as title

    if (window == NULL)                                                     // If no window created, terminates program
    {
        LOG_ERROR("Error creating window, terminating...\n");
        glfwTerminate();
        return false;
    }

    LOG_DEBUG("Window created");

    glfwMakeContextCurrent(window);                                         // Sets window to be used

    if (glewInit() != GLEW_OK)                                              // Inits glew, terminates if not
    {
        LOG_ERROR("Failed to init glew, terminating...\n");
        glfwTerminate();
        return false;
    }

    LOG_DEBUG("Glew initiated");

    glViewport(0, 0, 600, 800);                                             // Sets the viewport coordinates and size
    glfwSetFramebufferSizeCallback(window, windowsize_callback);            // Sets what function to call when screen gets resized
    glClearColor(0.7, 0.7, 0.7, 0.7);                                       // Sets a clear color / grey here
    glClear(GL_COLOR_BUFFER_BIT);                                           // Clears buffer
    glfwSwapBuffers(window);                                                // Swaps buffer
    glClear(GL_COLOR_BUFFER_BIT);                                           // Clears buffer

    //std::cout << "Init successful\n";
    LOG_DEBUG("Initialization successful");
    return true;
}

bool initShader(Shader& shader, const char* vertexPath, const char* fragmentPath)
{
    std::string vertexCode;
    if (!mad::readStringFromFile(vertexPath, vertexCode))
    {
        LOG_ERROR("ERROR::INIT::VERTEX... terminating\n");
        glfwTerminate();
        return false;
    }

    LOG_DEBUG("Loaded vertex code from %s", vertexPath);

    std::string fragmentCode;
    if (!mad::readStringFromFile(fragmentPath, fragmentCode))
    {
        LOG_ERROR("ERROR::INIT::FRAGMENT... terminating\n");
        glfwTerminate();
        return false;
    }

    LOG_DEBUG("Loaded fragment code from %s", fragmentPath);

    if (!shader.createProgram(vertexCode, fragmentCode))
    {
        LOG_ERROR("ERROR::SHADER::INIT... terminating... \n");
        glfwTerminate();
        return false;
    }

    LOG_DEBUG("Created shader program");
    return true;
}

bool initMap(const char* mapPath, std::vector<Wall>& walls, GLuint texture)
{
    std::string mapData;

    if (!mad::readStringFromFile(mapPath, mapData))
    {
        LOG_ERROR("ERROR::MAP::INIT... terminating...\n");
        glfwTerminate();
        return false;
    }
    
    int x = 0;
    int y = 35;

    std::vector<std::vector<int>> temp;
    std::vector<int> row;
  
    for (char& c : mapData)
    {
        switch (c)
        {
        case '1':
            walls.push_back(Wall(glm::vec3(x, y, 0), 1.0f, texture));
            row.push_back(1);
            LOG_DEBUG("Pushed back wall at coordinates: %d \t%d", x, y);
            x++;
            break;
        case '\n':
            x = 0;
            temp.push_back(row);
            row.clear();
            y--;
            break;
        case '0':
            row.push_back(0);
            x++;
            break;
        case '2':
            row.push_back(0);
            x++;
            break;
        }
    }
    for (int i = temp.size() - 1; i >= 0; i--)
    {
        tileMap.push_back(temp[i]);
    }
    
    for (int i = 0; i < tileMap.size(); i++)
    {
        for (int j = 0; j < tileMap[i].size(); j++)
        {
            std::cout << tileMap[i][j] << " ";
        }
        std::cout << "\n";
    }
    
    
    LOG_DEBUG("Loaded and created map");

    triangleShader.use();
    view = glm::lookAt(glm::vec3(14.0f, 18.0f, 50.f), glm::vec3(14.0f, 18.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    float height, width;
    glfwGetWindowSize(window, (int*)&width, (int*)&height);
    projection = glm::perspective(glm::radians(45.0f), width / height, 0.1f, 100.0f);

    triangleShader.setMatrix("view", view);
    triangleShader.setMatrix("projection", projection);

    LOG_DEBUG("Set view and projection");
    return true;
}

void windowsize_callback(GLFWwindow* window, int height, int width)
{
    glViewport(0, 0, height, width);
    triangleShader.use();
    glfwGetWindowSize(window, &width, &height);
    projection = glm::perspective(glm::radians(45.0f), (float)width / (float)height, 0.1f, 100.0f);
    triangleShader.setMatrix("projection", projection);
}