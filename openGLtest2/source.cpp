#include "Init.h"
#include "Shaders.h"
#include "Wall.h"
#include "Texture.h"
#include "logger.h"
#include "Pacman.h"

int speed = 4;

GLFWwindow* window;
Shader triangleShader;
glm::mat4 view(1.0f);
glm::mat4 projection(1.0f);
std::vector<std::vector<int>> tileMap;

int vertexPerTriangle = 9;

void processInput(GLFWwindow* window, double deltaTime, Pacman &pacman);

int main()
{
    if (!initWindow()) return -1;

    if (!initShader(triangleShader, "vertex.vert", "fragment.frag")) return -1;

    std::vector<Wall> level1;

    Texture wallTexture = Texture("container.jpg");
    if (!initMap("level0", level1, wallTexture.getID())) return -1;

    Texture pacmanTexture = Texture("pacman.png");
    Pacman pacman = Pacman(glm::vec3(0.0f, 18.0f, 0.0f), pacmanTexture.getID(), tileMap);
    double currentFrame = glfwGetTime();
    double lastFrame = currentFrame;

    while (!glfwWindowShouldClose(window))
    {
        // Input
        currentFrame = glfwGetTime();
        double deltaTime = currentFrame - lastFrame;
        if (deltaTime > 1.0 / 20)
        {
            lastFrame = currentFrame;


            processInput(window, deltaTime, pacman);
            pacman.move(deltaTime);


            //Rendering
            glClear(GL_COLOR_BUFFER_BIT);

            for (size_t i = 0; i < level1.size(); i++)
            {
                level1[i].display();
            }

            pacman.display();

            // Event checking, swap buffers
            glfwSwapBuffers(window);
            glfwPollEvents();
        }
    }

    glfwTerminate();
    return 0;
}

void processInput(GLFWwindow* window, double deltaTime, Pacman &pacman)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
    {
        LOG_DEBUG("Pressed UP");
        pacman.changeDirection('U', deltaTime);
    }
    else if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
    {
        LOG_DEBUG("Pressed DOWN");
        pacman.changeDirection('D', deltaTime);
    }
    else if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
    {
        LOG_DEBUG("Pressed LEFT");
        pacman.changeDirection('L', deltaTime);
    }
    else if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
    {
        LOG_DEBUG("Pressed RIGHT");
        pacman.changeDirection('R', deltaTime);
    }
}