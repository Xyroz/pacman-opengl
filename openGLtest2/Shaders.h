#pragma once
#include <iostream>

#include "Dependencies\glew-2.1.0\include\GL\glew.h"
#include "Dependencies\glfw-3.2.1.bin.WIN32\include\GLFW\glfw3.h"
#include "Dependencies\glm\glm\glm.hpp"
#include "Dependencies\glm\glm\gtc\matrix_transform.hpp"
#include "Dependencies\glm\glm\gtc\type_ptr.hpp"

class Shader
{
private:    
    GLuint programNo;
public:
    bool createProgram(const std::string& vertexString, const std::string& fragmentString);

    void use();

    void setInt(const std::string& name, GLint value);
    void setInt(const std::string& name, GLint value, GLint value2);
    void setInt(const std::string& name, GLint value, GLint value2, GLint value3);
    void setInt(const std::string& name, GLint value, GLint value2, GLint value3, GLint value4);

    void setFloat(const std::string& name, GLfloat value);
    void setFloat(const std::string& name, GLfloat value, GLfloat value2);
    void setFloat(const std::string& name, GLfloat value, GLfloat value2, GLfloat value3);
    void setFloat(const std::string&name, GLfloat value, GLfloat value2, GLfloat value3, GLfloat value4);

    void setMatrix(const std::string& name, glm::mat4 trans);
};