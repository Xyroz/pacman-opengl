#include "Texture.h"
#include "logger.h"
#include "Dependencies\STB_image\stb_image.h"

Texture::Texture(const char* path)
{
    glGenTextures(1, &textureID);                                                     // Generates texture
    glBindTexture(GL_TEXTURE_2D, textureID);                                          // Binds texture for use

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);          // Sets texture to clamp to border on S-axis
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);          // Sets texture to clamp to border on T-axis
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);               // Sets filter type to linear
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);               // Sets filter type to linear

    stbi_set_flip_vertically_on_load(true);
    int width, height, nrChannels;
    unsigned char* data;
    if (path == "pacman.png")
    {
        data = stbi_load(path, &width, &height, &nrChannels, 3);// Loads image
    }
    else
    {
        data = stbi_load(path, &width, &height, &nrChannels, 0);// Loads image
    }

    if (!data)
    {
        LOG_WARN("ERROR::TEXTURE::READ_FILE");
    }
    // Inserts image data into current texture bound to GL_TEXTURE_2D
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);                                                // Generates mipmap
    stbi_image_free(data);
    LOG_DEBUG("Successfully read and created texture from %s", path);
}

GLuint Texture::getID()
{
    return textureID;
}